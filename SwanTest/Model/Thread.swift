//
//  Thread.swift
//  SwanTest
//
//  Created by Leitli Arnold on 01/07/16.
//  Copyright © 2016 LeitCom. All rights reserved.
//

import Foundation

typealias Callback = () -> ()

struct QOS {
    static let Background = dispatch_get_global_queue(QOS_CLASS_BACKGROUND, 0)
    static let UserInteractive = dispatch_get_global_queue(QOS_CLASS_USER_INTERACTIVE, 0)
    static let UserInitiated = dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0)
    static let Utility = dispatch_get_global_queue(QOS_CLASS_UTILITY, 0)
    static let Default = dispatch_get_global_queue(QOS_CLASS_DEFAULT, 0)
}

let MainQueue = dispatch_get_main_queue()

class Thread {
    static func runBackground(block: Callback, complete: Callback) {
        runBackground {
            block()
            runMain({
                complete()
            })
        }
    }

    static func runBackgroundAfter(delay: UInt64, block: Callback, complete: Callback) {
        backgroundAfter(delay) {
            block()
            runMain({
                complete()
            })
        }
    }

    static func runBackground(block: Callback) {
        run(QOS.Background, block: block)
    }

    static func runUserInteractive(block: Callback) {
        run(QOS.UserInteractive, block: block)
    }

    static func runUserInitiated(block: Callback) {
        run(QOS.UserInitiated, block: block)
    }

    static func runUtility(block: Callback) {
        run(QOS.Utility, block: block)
    }

    static func runDefault(block: Callback) {
        run(QOS.Default, block: block)
    }

    static func runMain(block: Callback) {
        run(MainQueue, block: block)
    }

    static func run(onQueue: dispatch_queue_t, block: Callback) {
        dispatch_async(onQueue, block)
    }

    static func backgroundAfter(delay: UInt64, block: Callback) {
        after(delay, onQueue: QOS.Background, block: block)
    }

    static func mainAfter(delay: UInt64, block: Callback) {
        after(delay, onQueue: MainQueue, block: block)
    }

    static func after(delay: UInt64, onQueue: dispatch_queue_t, block: Callback) {
        dispatch_after(delay, onQueue, block)
    }
}
