//
//  Movie.swift
//  SwanTest
//
//  Created by Leitli Arnold on 02/07/16.
//  Copyright © 2016 LeitCom. All rights reserved.
//

import Foundation

class Movie {
    internal var necessaryProperties: [String] = [
        "adult",
        "backdropPath",
        "genreIDs",
        "id",
        "originalLanguage",
        "originalTitle",
        "overview",
        "popularity",
        "posterPath",
        "releaseDate",
        "title",
        "video",
        "voteAverage",
        "voteCount",
    ]

    let adult: Bool
    let backdropPath: String
    let genreIDs: [Int]
    let id: Int
    let originalLanguage: String
    let originalTitle: String
    let overview: String
    let popularity: Double
    let posterPath: String
    let releaseDate: NSDate
    let title: String
    let video: Int
    let voteAverage: Float
    let voteCount: Int

    init (adult: Bool, backdropPath: String, genreIDs: [Int], id: Int, originalLanguage: String, originalTitle: String, overview: String, popularity: Double, posterPath: String, releaseDate: NSDate, title: String, video: Int, voteAverage: Float, voteCount: Int) {
        self.adult = adult
        self.backdropPath = backdropPath
        self.genreIDs = genreIDs
        self.id = id
        self.originalTitle = originalTitle
        self.originalLanguage = originalLanguage
        self.overview = overview
        self.popularity = popularity
        self.posterPath = posterPath
        self.releaseDate = releaseDate
        self.title = title
        self.video = video
        self.voteAverage = voteAverage
        self.voteCount = voteCount
    }

    static func parse(object: [String : AnyObject]) -> Movie? {

        guard let adultValue = object["adult"] as? Bool else {
            print("adult")
            return nil
        }

        let backdropPathValue = object["backdrop_path"] as? String ?? ""

        let genreIDsValue = object["genre_ids"] as? [Int] ?? [Int]()

        guard let idValue = object["id"] as? Int else {
            return nil
        }


        guard let originalLanguageValue = object["original_language"] as? String else {
            print("original_language")
            return nil
        }

        guard let originalTitleValue = object["original_title"] as? String else {
            print("original_title")
            return nil
        }

        guard let overviewValue = object["overview"] as? String else {
            print("overview")
            return nil
        }


        guard let popularityValue = object["popularity"] as? Double else {
            print("popularity")
            return nil
        }

        let posterPathValue = object["poster_path"] as? String ?? ""

//        guard let releaseDateValue = object["release_date"] as? NSDate else {
//            return nil
//        }

        guard let titleValue = object["title"] as? String else {
            print("title")
            return nil
        }

        guard let videoValue = object["video"] as? Int else {
            print("video")
            return nil
        }

        guard let voteAverageValue = object["vote_average"] as? Float else {
            print("vote_average")
            return nil
        }

        guard let voteCountValue = object["vote_count"] as? Int else {
            print("vote_count")
            return nil
        }
        return Movie(adult: adultValue, backdropPath: backdropPathValue, genreIDs: genreIDsValue, id: idValue, originalLanguage: originalLanguageValue, originalTitle: originalTitleValue, overview: overviewValue, popularity: popularityValue, posterPath: posterPathValue, releaseDate: NSDate(), title: titleValue, video: videoValue, voteAverage: voteAverageValue, voteCount: voteCountValue)
    }

    static func parse(array: [[String: AnyObject]]) -> [Movie] {
        var tempArray = [Movie]()

        for item in array {
            if let parsedItem = parse(item) {
                tempArray.append(parsedItem)
            }
        }
        return tempArray
    }


}
