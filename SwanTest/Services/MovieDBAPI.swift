//
//  MovieDBAPI.swift
//  SwanTest
//
//  Created by Leitli Arnold on 30/06/16.
//  Copyright © 2016 LeitCom. All rights reserved.
//

import Foundation

enum Reference: String {
    case Configuration = "configuration"
    case Account = "account"
    case Authentication = "authentication"
    case Certications = "certification"
    case Collections = "collection"
    case Companies = "company"
    case Credits = "credit"
    case Discover = "discover"
    case Find = "find"
    case Genres = "genre"
    case GuestSessions = "guest_session"
    case Jobs = "job"
    case Keywords = "keyword"
    case Lists = "list"
    case Movie = "movie"
    case Networks = "network"
    case People = "person"
    case Review = "review"
    case Search = "search"
    case Timezones = "timezones"
    case TV = "tv"
}

class MovieDBAPI {
    static let shared = MovieDBAPI()

    private let baseURL = "https://api.themoviedb.org/"
    private let imageURL = "http://image.tmdb.org/t/p/w500"
    private let APIversion = "3"
    private let APIKey = "?api_key=\(Config.shared.MovieDBAPIKey)"

    var configuration: NSURL {
        return url(.Configuration, parameters:[:])
    }

    var movieUpcoming: NSURL {
        return url(.Movie, subreference: "upcoming", parameters:[:])
    }

    var movieLatest: NSURL {
        return url(.Movie, subreference: "latest", parameters:[:])
    }

    var moviePopular: NSURL {
        return url(.Movie, subreference: "popular", parameters:[:])
    }

    func movieUpcoming(page: Int) -> NSURL {
        return url(.Movie, subreference: "upcoming", parameters:["page":"\(page)"])
    }


    func moviePopular(page: Int) -> NSURL {
        return url(.Movie, subreference: "popular", parameters:["page":"\(page)"])
    }

    func movie(id: String, parameters: [String:String]) -> NSURL {
        return url(.Movie, subreference: nil, id: id, parameters: [:])
    }

    func file(relativePath: String, parameters: [String:String]) -> NSURL {
        return url(relativePath, parameters:parameters)
    }

    private func url(reference: Reference, parameters: [String:String]) -> NSURL {
        return url(reference, subreference: nil, id: nil, parameters:parameters)
    }

    private func url(reference: Reference, id: String?, parameters: [String:String]) -> NSURL {
        return url(reference, subreference: nil, id: id, parameters:parameters)
    }

    private func url(reference: Reference, subreference: String?, parameters: [String:String]) -> NSURL {
        return url(reference, subreference: subreference, id: nil, parameters:parameters)
    }

    private func url(reference: Reference, subreference: String?, id: String?, parameters: [String:String]) -> NSURL {

        var path = "/" + reference.rawValue
        if let unwrappedID = id {
            path += "/" + unwrappedID
        }

        if let unwrappedSubreference = subreference {
            path += "/" + unwrappedSubreference
        }
        return url(path, parameters: parameters)
    }

    private func url(path: String, parameters: [String:String]) -> NSURL {
        var url = baseURL + APIversion + path
        url += APIKey
        for key in parameters.keys {
            url += "&" + key + "=" + parameters[key]!
        }
        print(url)
        return NSURL(string: url)!
    }

    func imageUrl(relativePath: String) -> NSURL {
        let url = imageURL + relativePath
        return NSURL(string: url)!
    }
}
