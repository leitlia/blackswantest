//
//  Data.swift
//  SwanTest
//
//  Created by Leitli Arnold on 02/07/16.
//  Copyright © 2016 LeitCom. All rights reserved.
//

import Foundation

class DataManager {
    static let shared = DataManager()

    var upcomingMovies = [Movie]()
    var latestMovies = [Movie]()
    var popularMovies = [Movie]()
}
