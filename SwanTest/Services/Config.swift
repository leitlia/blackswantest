//
//  Config.swift
//  SwanTest
//
//  Created by Leitli Arnold on 30/06/16.
//  Copyright © 2016 LeitCom. All rights reserved.
//

import Foundation

private enum Keys: String {
    case MovieDBAPIKey = "MovieDBAPIKey"
}

class Config {
    static let shared = Config()
    private var _localConfig = NSDictionary()

    var MovieDBAPIKey: String {
        return _localConfig[Keys.MovieDBAPIKey.rawValue] as! String
    }

    init () {
        readConfig()
    }

    func readConfig() {
        if let path = NSBundle.mainBundle().pathForResource("config", ofType: "plist") {
            _localConfig = NSDictionary(contentsOfFile: path)!
        }
    }
}
