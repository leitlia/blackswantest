//
//  Network.swift
//  SwanTest
//
//  Created by Leitli Arnold on 30/06/16.
//  Copyright © 2016 LeitCom. All rights reserved.
//

import Foundation

typealias successBlock = (response: AnyObject) -> ()
typealias failureBlock = (error: NSError) -> ()

class Network {
    static let shared = Network()

    func getConfiguration(success: successBlock, failure: failureBlock) {
        get(MovieDBAPI.shared.configuration, success: success, failure: failure)
    }

    func getMovie(page: String, success: successBlock, failure: failureBlock) {
        get(MovieDBAPI.shared.movie(page, parameters: [:]), success: success, failure: failure)
    }

    func get(path: NSURL, success: successBlock, failure: failureBlock) {
        httpRequest(method: "GET", path: path, body: nil, success: success, failure: failure)
    }

    func post(path: NSURL, success: successBlock, failure: failureBlock) {
        httpRequest(method: "POST", path: path, body: nil, success: success, failure: failure)
    }

    private func httpRequest(method method: String, path: NSURL, body: [String: AnyObject]?, success: successBlock, failure: failureBlock) {
        let request = NSMutableURLRequest(URL: path)
        request.HTTPMethod = method
        let session = NSURLSession.sharedSession()

        do {
            if let unwrapBody = body {
                let jsonBody = try NSJSONSerialization.dataWithJSONObject(unwrapBody, options: .PrettyPrinted)
                request.HTTPBody = jsonBody
            }
            let task = session.dataTaskWithRequest(request, completionHandler: { data, response, error -> Void in
                if let unwrappedData = data {
                    do {
                        let json = try NSJSONSerialization.JSONObjectWithData(unwrappedData, options: NSJSONReadingOptions.AllowFragments) as! [String: AnyObject]
                        success(response: json)
                    } catch {
                        Thread.runMain({
                            success(response: ["imageData": unwrappedData])
                        })
//                        failure(error: NSError(domain: "error.network.JSONObjectWithData", code: -1, userInfo: ["localizedDescription": "Json data formation error"]))
                    }
                } else {
                    failure(error: error!)
                }
            })
            task.resume()
        } catch {
            failure(error: NSError(domain: "error.network.dataWithJSONObject", code: -1, userInfo: ["localizedDescription": "Json Body formation error"]))
        }
    }
}
