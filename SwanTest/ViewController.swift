//
//  ViewController.swift
//  SwanTest
//
//  Created by Leitli Arnold on 30/06/16.
//  Copyright © 2016 LeitCom. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        Network.shared.get(MovieDBAPI.shared.movieUpcoming, success: { (response) in
            DataManager.shared.upcomingMovies = Movie.parse(response["results"] as! [[String: AnyObject]])
            self.performSegueWithIdentifier("UpComingSegueIdentifier", sender: self)
            }) { (error) in
                print(error)
        }

        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}
