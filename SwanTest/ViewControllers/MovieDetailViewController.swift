//
//  MovieDetailViewController.swift
//  SwanTest
//
//  Created by Leitli Arnold on 03/07/16.
//  Copyright © 2016 LeitCom. All rights reserved.
//

import UIKit

class MovieDetailViewController: UIViewController {
    @IBOutlet weak var backgroundImage: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var overviewTextView: UITextView!
    var movie: Movie!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.backgroundImage.setImageBy(MovieDBAPI.shared.imageUrl(movie.backdropPath))
        self.overviewTextView.text = movie.overview
        self.titleLabel.text = movie.title
        self.ratingLabel.text = "\(movie.voteAverage)/5"
    }
}
