//
//  UpComingViewController.swift
//  SwanTest
//
//  Created by Leitli Arnold on 02/07/16.
//  Copyright © 2016 LeitCom. All rights reserved.
//

import UIKit

private let upcomingLoadingMovieReuseIdentifier = "loadingMovieReuseIdentifier"
private let upcomingMovieCellReuseIdentifier = "upcomingMovieCellReuseIdentifier"
private let movieDetailSegueIdentifier = "MovieDetailSegueIdentifier"

class UpComingViewController: UICollectionViewController {
    var selectedMovie: Movie!
    var pageCount = 1

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.title = "UPCOMING"
    }

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        super.prepareForSegue(segue, sender: sender)
        if segue.identifier == movieDetailSegueIdentifier {
            let viewController: MovieDetailViewController = segue.destinationViewController as! MovieDetailViewController
            viewController.movie = self.selectedMovie

        }
    }
}

extension UpComingViewController {
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return DataManager.shared.upcomingMovies.count + 1
    }

    override func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        let movie = DataManager.shared.upcomingMovies[indexPath.row]
        Network.shared.getMovie("\(movie.id)", success: { (response) in
            Thread.runMain({
                self.selectedMovie = Movie.parse(response as! [String: AnyObject])
                self.performSegueWithIdentifier(movieDetailSegueIdentifier, sender: self)
            })
        }) { (error) in
        }
    }

    override func collectionView(collectionView: UICollectionView, willDisplayCell cell: UICollectionViewCell, forItemAtIndexPath indexPath: NSIndexPath) {
        if indexPath.row == DataManager.shared.upcomingMovies.count {
            Network.shared.get(MovieDBAPI.shared.movieUpcoming(pageCount + 1), success: { (response) in
                Thread.runMain({
                self.pageCount += 1
                DataManager.shared.upcomingMovies += Movie.parse(response["results"] as! [[String: AnyObject]])
                collectionView.reloadData()
                    })
            }) { (error) in
            }
        }
    }

    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        if indexPath.row == DataManager.shared.upcomingMovies.count {
            let cell = collectionView.dequeueReusableCellWithReuseIdentifier(upcomingLoadingMovieReuseIdentifier, forIndexPath: indexPath) as! LoadingMovieCell
            return cell
        }
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(upcomingMovieCellReuseIdentifier, forIndexPath: indexPath) as! UpComingMovieCell
        let movie = DataManager.shared.upcomingMovies[indexPath.row]
        cell.titleLabel.text = movie.title
        cell.posterImage.setImageBy(MovieDBAPI.shared.imageUrl(movie.posterPath))
        return cell
    }

}
