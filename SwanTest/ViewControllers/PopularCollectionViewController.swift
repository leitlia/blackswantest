//
//  PopularCollectionViewController.swift
//  SwanTest
//
//  Created by Leitli Arnold on 03/07/16.
//  Copyright © 2016 LeitCom. All rights reserved.
//

import UIKit

private let popularMovieReuseIdentifier = "PopularMovieCellReuseIdentifier"
private let popularLoadingReuseIdentifier = "PopularLoadingReuseIdentifier"
private let movieDetailSegueIdentifier = "MovieDetailSegueIdentifier"

class PopularCollectionViewController: UICollectionViewController {
    var selectedMovie: Movie!
    var pageCount = 1

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        super.prepareForSegue(segue, sender: sender)
        if segue.identifier == movieDetailSegueIdentifier {
            let viewController: MovieDetailViewController = segue.destinationViewController as! MovieDetailViewController
            viewController.movie = self.selectedMovie

        }
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        Network.shared.get(MovieDBAPI.shared.moviePopular, success: { (response) in
            Thread.runMain({
                DataManager.shared.popularMovies = Movie.parse(response["results"] as! [[String: AnyObject]])
                print(DataManager.shared.popularMovies)
                self.collectionView?.reloadData()
            })
            }) { (error) in

        }
    }

}

extension PopularCollectionViewController {
    // MARK: UICollectionViewDataSource

    override func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }


    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if DataManager.shared.popularMovies.count == 0 {
            return 0
        }
        return DataManager.shared.popularMovies.count + 1
    }

    override func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        let movie = DataManager.shared.popularMovies[indexPath.row]
        Network.shared.getMovie("\(movie.id)", success: { (response) in
            Thread.runMain({
                self.selectedMovie = Movie.parse(response as! [String: AnyObject])
                self.performSegueWithIdentifier(movieDetailSegueIdentifier, sender: self)
            })
        }) { (error) in
        }
    }

    override func collectionView(collectionView: UICollectionView, willDisplayCell cell: UICollectionViewCell, forItemAtIndexPath indexPath: NSIndexPath) {
        if indexPath.row == DataManager.shared.popularMovies.count {
            Network.shared.get(MovieDBAPI.shared.moviePopular(pageCount + 1), success: { (response) in
                Thread.runMain({
                    self.pageCount += 1
                    DataManager.shared.popularMovies += Movie.parse(response["results"] as! [[String: AnyObject]])
                    collectionView.reloadData()
                })
            }) { (error) in
            }
        }
    }

    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        if indexPath.row == DataManager.shared.popularMovies.count {
            let cell = collectionView.dequeueReusableCellWithReuseIdentifier(popularLoadingReuseIdentifier, forIndexPath: indexPath)
            return cell
        }

        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(popularMovieReuseIdentifier, forIndexPath: indexPath) as! PopularCollectionViewCell
        let movie = DataManager.shared.popularMovies[indexPath.row]
        cell.titleLabel.text = movie.title
        cell.backgroundImage.setImageBy(MovieDBAPI.shared.imageUrl(movie.backdropPath))
        cell.descriptionLabel.text = movie.overview
        return cell
    }
}
