//
//  LatestTableViewController.swift
//  SwanTest
//
//  Created by Leitli Arnold on 03/07/16.
//  Copyright © 2016 LeitCom. All rights reserved.
//

import UIKit

class LatestTableViewController: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "LATEST"

    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)

        Network.shared.get(MovieDBAPI.shared.movieLatest, success: { (response) in
            Thread.runMain({
                if let movie = Movie.parse(response as! [String:AnyObject]) {
                    DataManager.shared.latestMovies.append(movie)
                }
                self.tableView.reloadData()
            })
            }) { (error) in
        }
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print(1)
        return DataManager.shared.latestMovies.count
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        print(2)
        let cell = tableView.dequeueReusableCellWithIdentifier("latestReuseIdentifier", forIndexPath: indexPath) as! LatestTableViewCell
        let movie = DataManager.shared.latestMovies[indexPath.row]

        cell.titleLabel.text = movie.title
        cell.numberLabel.text = "\(indexPath.row)."
        cell.miniImage.setImageBy(MovieDBAPI.shared.imageUrl(movie.posterPath))

        return cell
    }

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }

}
