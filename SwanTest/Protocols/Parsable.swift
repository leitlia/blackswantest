//
//  Parsable.swift
//  SwanTest
//
//  Created by Leitli Arnold on 02/07/16.
//  Copyright © 2016 LeitCom. All rights reserved.
//

import Foundation

protocol Parsable {
    var necessaryProperties: [String] { get }

    static func parse(object: [String:AnyObject]) -> Self
    static func parse(array: [[String:AnyObject]]) -> [Self]
}

//extension Parsable {
//    static func parse(object: [String:AnyObject]) -> Self {
//        return self
//    }
//
//    static func parse(array: [[String:AnyObject]]) -> [Self] {
//        return [Self]
//    }
//}
