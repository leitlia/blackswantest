//
//  UIImageView.swift
//  SwanTest
//
//  Created by Leitli Arnold on 02/07/16.
//  Copyright © 2016 LeitCom. All rights reserved.
//

import UIKit

extension UIImageView {
    func setImageBy(url: NSURL) {

        Network.shared.get(url, success: { (response) in
            Thread.runMain({
                self.image = UIImage(data: response["imageData"] as! NSData)
            })

            }) { (error) in
                print(error)
        }
    }
}
