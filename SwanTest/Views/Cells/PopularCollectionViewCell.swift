//
//  PopularCollectionViewCell.swift
//  SwanTest
//
//  Created by Leitli Arnold on 03/07/16.
//  Copyright © 2016 LeitCom. All rights reserved.
//

import UIKit

class PopularCollectionViewCell: UICollectionViewCell {
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var descriptionLabel: UILabel!
    @IBOutlet var backgroundImage: UIImageView!

}
