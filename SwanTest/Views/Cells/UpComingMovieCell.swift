//
//  UpComingMovieCell.swift
//  SwanTest
//
//  Created by Leitli Arnold on 02/07/16.
//  Copyright © 2016 LeitCom. All rights reserved.
//

import UIKit

class UpComingMovieCell: UICollectionViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var posterImage: UIImageView!

}
