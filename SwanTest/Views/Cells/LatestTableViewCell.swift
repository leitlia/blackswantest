//
//  LatestTableViewCell.swift
//  SwanTest
//
//  Created by Leitli Arnold on 03/07/16.
//  Copyright © 2016 LeitCom. All rights reserved.
//

import UIKit

class LatestTableViewCell: UITableViewCell {

    @IBOutlet weak var numberLabel: UILabel!
    @IBOutlet weak var miniImage: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
